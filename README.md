#Jemalloc#
~~~
 Huge thank You to NickyD and the Firestorm team.
  Even though this library that is built has ben used
  for many years in programing to help control ram usage.
  
   NickyD of the "Pheonix" Firestorm team AKA "FS" was the first to
   implement it. 
   
   This library is called at startup time from the Linux 
   wrapper. 
   Even though this may sound simple it was ingenious way to 
   save the Linux viewers.
~~~   
 #My build#
~~~
 few years back watching the wonderful work of the FS team.
  I noticed the script change long before FS team posted the 
  3p-jemalloc I had constructed my own. 
  I have held off till today  Sun Apr 25 12:19:41 CDT 2021
  to post mine out of respect for the work of the team.
  
  A while back when they made the "3p" build script open
  I figured it was time to push mine. 
~~~ 
# Jemalloc Team#
~~~
  jemalloc is a general purpose malloc(3) implementation that emphasizes
fragmentation avoidance and scalable concurrency support.  jemalloc first came
into use as the FreeBSD libc allocator in 2005, and since then it has found its
way into numerous applications that rely on its predictable behavior.  In 2010
jemalloc development efforts broadened to include developer support features
such as heap profiling and extensive monitoring/tuning hooks.  Modern jemalloc
releases continue to be integrated back into FreeBSD, and therefore versatility
remains critical.  Ongoing development efforts trend toward making jemalloc
among the best allocators for a broad range of demanding applications, and
eliminating/mitigating weaknesses that have practical repercussions for real
world applications.

The COPYING file contains copyright and licensing information.

The INSTALL file contains information on how to configure, build, and install
jemalloc.

The ChangeLog file contains a brief summary of changes for each release.

URL: http://jemalloc.net/
~~~

# How to build # 

# git clone https://bitbucket.org/Drakeo/3p-jemalloc.git  
 cd  3p-jemalloc
 sh 3p_Buildme


    
